## Ansible роль для установки и настройки certbot

Роль позволяет установить в изолированное окружение и настроить самый 
популярный инструмент для работы с [Let`s Encrypt](https://letsencrypt.org)-
 провайдером бесплатных сертификатов ssl - [CertBot](https://certbot.eff.org).
 
 
### Требования для использования роли
Для использования роли должен быть установлен ansible не ниже версии 2.4.0.0.
Роль тестировалась только на python 2.7, однако, использование python 3.x 
ограничивается только ansible. С большой вероятностью, роль работоспособна 
для версии интерпретатора 3.x.


### Использование роли
Для более подробного назначения переменных роли, рекомендуется ознакомиться с
 комментариями в файле `defaults/main.yml`.
 
Установка зависимостей (перед использованием) производится:
```bash
ansible-galaxy install -r requirements.yml
```

Запуск роли производится:
```bash
ansible-playbook -i inventory.yml playbook.yml -vv
```
где `inventory.yml` - заранее подготовленный файл настроек, `playbook.yml` - 
подготовленный файл ansible playbook.


### Пример playbook для плагина dns
```yaml

- hosts: all
  roles:
    - '../../ansible_certbot'

  vars:
    certbot_plugins:
      - name: certbot_dns_reg_ru
        extra_index_url: https://pypi.bfg-soft.ru/simple

    certbot_pre_copy:
      # для запросов по DNS нужны будут учётные данные reg.ru в формате
      #
      #   certbot_dns_reg_ru:dns_reg_ru_username = LOGIN
      #   certbot_dns_reg_ru:dns_reg_ru_password = PASSWORD
      - src: "{{ inventory_dir }}/credentials.ini"
        dest: credentials.ini  # путь в папке certbot_data_home 

    certbot_domains:
      - name: test.bfg-group.ru
        preferred_challenges: dns-01
        options:
          - --authenticator certbot-dns-reg-ru:dns-reg-ru
          - --certbot-dns-reg-ru:dns-reg-ru-credentials "{{ certbot_data_home }}/credentials.ini"
          - --certbot-dns-reg-ru:dns-reg-ru-propagation-seconds 15
        renew_hook_deploy:
          type: command
          value: systemctl restart nginx
```
